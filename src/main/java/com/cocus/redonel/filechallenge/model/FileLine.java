package com.cocus.redonel.filechallenge.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class FileLine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String line;
    private Integer lineNumber;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "file_model_id")
    private FileModel fileModel;
}
