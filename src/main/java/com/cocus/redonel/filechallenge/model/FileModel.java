package com.cocus.redonel.filechallenge.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class FileModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String fileName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fileModel")
    private List<FileLine> lines;
    private int nbrLines;

    public void setLines(List<FileLine> lines) {
        this.lines = lines;
        this.nbrLines = lines != null ? lines.size() : 0;
    }

    public int getNbrLines() {
        return lines != null ? lines.size() : 0;
    }

}
