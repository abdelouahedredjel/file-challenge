package com.cocus.redonel.filechallenge.service.impl;

import com.cocus.redonel.filechallenge.dto.LineResponse;
import com.cocus.redonel.filechallenge.dto.LineWithMostCommonCharResponse;
import com.cocus.redonel.filechallenge.dto.ReverseLineResponse;
import com.cocus.redonel.filechallenge.exception.NoFileLineFoundException;
import com.cocus.redonel.filechallenge.helper.StringHelper;
import com.cocus.redonel.filechallenge.mapper.FileLineMapper;
import com.cocus.redonel.filechallenge.model.FileLine;
import com.cocus.redonel.filechallenge.repository.FileLineRepository;
import com.cocus.redonel.filechallenge.service.FileLineService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
@Slf4j
public class FileLineServiceImpl implements FileLineService {
    private final FileLineRepository fileLineRepository;
    private final FileLineMapper fileLineMapper;

    private static final int LIMIT_NUMBER_OF_LINES = 100;

    @Override
    public String getRandomLineText() {
        log.info("Getting random line as text");
        Optional<FileLine> fileLine = getRandomFileLine();
        if(fileLine.isPresent()) {
            log.info("Random line retrieved successfully");
            return fileLine.get().getLine();
        }
        else {
            log.warn("No file line is present in the database");
            throw new NoFileLineFoundException("No file line is present in the database");
        }
    }

    @Override
    public LineWithMostCommonCharResponse getRandomLine(){
        log.info("Getting random line with most common character");
        Optional<FileLine> fileLine = getRandomFileLine();
        if(fileLine.isPresent()) {
            Character mostCommonChar = StringHelper.findMostCommonChar(fileLine.get().getLine());
            log.info("Random line with most common character retrieved successfully");
            return fileLineMapper.mapToLineWithMostCommonCharResponse(fileLine.get(), mostCommonChar);
        } else {
            log.warn("No file line is present in the database");
            throw new NoFileLineFoundException("No file line is present in the database");
        }
    }

    @Override
    public ReverseLineResponse getRandomLineBackwards() {
        log.info("Getting random line backwards");
        Optional<FileLine> randomFileLine = getRandomFileLine();
        if(randomFileLine.isPresent()){
            String reversedLine = new StringBuilder(randomFileLine.get().getLine()).reverse().toString();
            log.info("Random line backwards retrieved successfully");
            return fileLineMapper.mapToReverseLineResponse(randomFileLine.get(), reversedLine);
        }else {
            log.warn("No file line is present in the database");
            throw new NoFileLineFoundException("No file line is present in the database");
        }
    }

    @Override
    public List<LineResponse> getLongestFileLines() {
        log.info("Getting longest file lines");
        List<FileLine> fileLines = fileLineRepository.findTopLinesOrderedByLineLengthDesc(LIMIT_NUMBER_OF_LINES);
        List<LineResponse> longestLines = fileLines.stream().map(fileLineMapper::mapToLineResponse).toList();
        log.info("Longest file lines retrieved successfully");
        return longestLines;
    }

    private Optional<FileLine> getRandomFileLine() {
        log.info("Getting random file line");
        long totalLines = fileLineRepository.count();
        if (totalLines == 0) {
            log.warn("No file lines present in the database");
            return Optional.empty();
        }
        Random random = new Random();
        long randomLineNumber = random.nextLong(totalLines);
        FileLine randomFileLine = fileLineRepository.findRandomLine(randomLineNumber);
        log.info("Random file line retrieved successfully");
        return Optional.of(randomFileLine);
    }
}
