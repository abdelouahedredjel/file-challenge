package com.cocus.redonel.filechallenge.service.impl;

import com.cocus.redonel.filechallenge.dto.LongestFileLinesResponse;
import com.cocus.redonel.filechallenge.exception.NoFileFoundException;
import com.cocus.redonel.filechallenge.model.FileLine;
import com.cocus.redonel.filechallenge.model.FileModel;
import com.cocus.redonel.filechallenge.repository.FileLineRepository;
import com.cocus.redonel.filechallenge.repository.FileRepository;
import com.cocus.redonel.filechallenge.service.FileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class FileServiceImpl implements FileService {

    private final FileRepository fileRepository;
    private final FileLineRepository fileLineRepository;
    private final int numberOfLinesToRetrieve;
    private final String sourceDir;

    public FileServiceImpl(FileRepository fileRepository,
                           FileLineRepository fileLineRepository,
                           @Value("${file.lines.retrieve.number}") int numberOfLinesToRetrieve,
                           @Value("${file.upload.source-dir}") String sourceDir) {
        this.fileRepository = fileRepository;
        this.fileLineRepository = fileLineRepository;
        this.numberOfLinesToRetrieve = numberOfLinesToRetrieve;
        this.sourceDir = sourceDir;
    }

    @Transactional
    @Override
    public void uploadFile(String fileName) throws IOException {
        log.info("Uploading file: {}", fileName);

        Path filePath = Paths.get(getSourceDir(), fileName);
        List<String> lines = Files.readAllLines(filePath);

        Optional<FileModel> existingFileOptional = fileRepository.findByFileName(fileName);

        FileModel fileModel;
        if (existingFileOptional.isPresent()) {
            fileModel = existingFileOptional.get();
            fileLineRepository.deleteByFileModel(fileModel);
            fileModel.getLines().clear();
        } else {
            fileModel = new FileModel();
            fileModel.setFileName(fileName);
        }

        List<FileLine> fileLines = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
            FileLine fileLine = FileLine.builder()
                    .line(lines.get(i))
                    .lineNumber(i + 1)
                    .fileModel(fileModel)
                    .build();
            fileLines.add(fileLine);
        }
        fileModel.setLines(fileLines);

        fileRepository.save(fileModel);

        log.info("File uploaded successfully: {}", fileName);
    }

    @Override
    public LongestFileLinesResponse getLimitedNumberOfLongestLinesOfRandomFile() {
        log.info("Retrieving {} longest lines of random file", numberOfLinesToRetrieve);

        FileModel randomFile = fileRepository.findRandomFile().orElseThrow(() -> {
            log.error("No files found");
            return new NoFileFoundException("No files found");
        });

        List<String> longestLines = randomFile.getLines().stream()
                .map(FileLine::getLine)
                .sorted(Comparator.comparingInt(String::length).reversed())
                .limit(numberOfLinesToRetrieve)
                .toList();

        log.info("{} longest lines retrieved successfully", numberOfLinesToRetrieve);

        return LongestFileLinesResponse.builder()
                .fileName(randomFile.getFileName())
                .longestLines(longestLines)
                .build();
    }

    private String getSourceDir() {
        String projectDir = System.getProperty("user.dir");
        return Paths.get(projectDir, sourceDir).toString();
    }
}
