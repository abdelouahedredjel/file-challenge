package com.cocus.redonel.filechallenge.service;

import com.cocus.redonel.filechallenge.dto.LineResponse;
import com.cocus.redonel.filechallenge.dto.LineWithMostCommonCharResponse;
import com.cocus.redonel.filechallenge.dto.ReverseLineResponse;

import java.util.List;

public interface FileLineService {

    String getRandomLineText();

    LineWithMostCommonCharResponse getRandomLine();

    ReverseLineResponse getRandomLineBackwards();

    List<LineResponse> getLongestFileLines();
}
