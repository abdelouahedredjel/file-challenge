package com.cocus.redonel.filechallenge.service;

import com.cocus.redonel.filechallenge.dto.LongestFileLinesResponse;

import java.io.IOException;

public interface FileService {
    void uploadFile(String fileName) throws IOException;

    LongestFileLinesResponse getLimitedNumberOfLongestLinesOfRandomFile();
}
