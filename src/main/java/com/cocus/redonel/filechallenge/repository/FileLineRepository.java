package com.cocus.redonel.filechallenge.repository;

import com.cocus.redonel.filechallenge.model.FileLine;
import com.cocus.redonel.filechallenge.model.FileModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileLineRepository extends JpaRepository<FileLine, Long> {

    @Query(value = "SELECT * FROM file_line OFFSET :lineNumber LIMIT 1", nativeQuery = true)
    FileLine findRandomLine(@Param("lineNumber") long lineNumber);
    @Query(value = "SELECT * FROM file_line ORDER BY CHAR_LENGTH(line) DESC LIMIT :numberOfLines", nativeQuery = true)
    List<FileLine> findTopLinesOrderedByLineLengthDesc(@Param("numberOfLines") long numberOfLines);

    void deleteByFileModel(FileModel fileModel);
}
