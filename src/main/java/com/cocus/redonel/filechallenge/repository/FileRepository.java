package com.cocus.redonel.filechallenge.repository;

import com.cocus.redonel.filechallenge.model.FileModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileRepository extends JpaRepository<FileModel, Long> {

    @Query(value = "SELECT * FROM file_model OFFSET floor(random() * (SELECT COUNT(*) FROM file_model)) LIMIT 1",
            nativeQuery = true)
    Optional<FileModel> findRandomFile();

    Optional<FileModel> findByFileName(String fileName);
}
