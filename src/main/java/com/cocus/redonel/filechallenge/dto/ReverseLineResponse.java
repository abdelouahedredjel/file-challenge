package com.cocus.redonel.filechallenge.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReverseLineResponse {

    private LineResponse lineResponse;
    private String reverseLine;

}
