package com.cocus.redonel.filechallenge.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LongestFileLinesResponse {
    private String fileName;
    private List<String> longestLines;
}
