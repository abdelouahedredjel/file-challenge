package com.cocus.redonel.filechallenge.controller;

import com.cocus.redonel.filechallenge.dto.LongestFileLinesResponse;
import com.cocus.redonel.filechallenge.exception.NoFileFoundException;
import com.cocus.redonel.filechallenge.service.FileService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@Slf4j
public class FileController {

    private final FileService fileService;

    @PostMapping("/upload-file")
    public ResponseEntity<String> uploadFile(@RequestParam String fileName){
        try {
            log.info("Received request to upload file: {}", fileName);
            fileService.uploadFile(fileName);
            return ResponseEntity.ok("File uploaded successfully.");
        } catch (IOException e) {
            log.error("Error uploading file: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error uploading file: " + e.getMessage());
        }
    }

    @GetMapping("/random-file/longest-lines")
    public ResponseEntity<LongestFileLinesResponse> getLimitedNumberOfLongestLinesOfRandomFile() {
        try {
            log.info("Received request to retrieve some of longest lines of random file");
            LongestFileLinesResponse response = fileService.getLimitedNumberOfLongestLinesOfRandomFile();
            return ResponseEntity.ok(response);
        } catch (NoFileFoundException e) {
            log.warn("No file found");
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            log.error("Internal server error: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
