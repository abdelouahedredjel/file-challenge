package com.cocus.redonel.filechallenge.controller;

import com.cocus.redonel.filechallenge.dto.LineResponse;
import com.cocus.redonel.filechallenge.dto.LineWithMostCommonCharResponse;
import com.cocus.redonel.filechallenge.dto.ReverseLineResponse;
import com.cocus.redonel.filechallenge.exception.NoFileLineFoundException;
import com.cocus.redonel.filechallenge.service.FileLineService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@Slf4j
@RestController
public class FileLineController {

    private final FileLineService fileLineService;

    @GetMapping(value = "/random-line-text", produces = "text/plain")
    public ResponseEntity<String> getRandomLineText() {
        try{
            log.info("Received request to get random line as text");
            String randomLine = fileLineService.getRandomLineText();
            log.info("Random line retrieved successfully");
            return ResponseEntity.status(HttpStatus.OK).body(randomLine);
        }catch (NoFileLineFoundException e){
            log.warn("No file line found");
            return ResponseEntity.notFound().build();
        }catch (Exception e){
            log.error("Internal server error: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping(value = "/random-line-json", produces = "application/json")
    public ResponseEntity<LineWithMostCommonCharResponse> getRandomLineAsJson() {
        return getRandomLine();
    }

    @GetMapping(value = "/random-line-xml", produces = "application/xml")
    public ResponseEntity<LineWithMostCommonCharResponse> getRandomLineAsXml() {
        return getRandomLine();
    }

    @GetMapping(value = "/random-line-backwards")
    public ResponseEntity<ReverseLineResponse> getRandomLineBackwardsText() {
        try{
            log.info("Received request to get random line backwards");
            ReverseLineResponse randomLineBackwards = fileLineService.getRandomLineBackwards();
            log.info("Random line backwards retrieved successfully");
            return ResponseEntity.status(HttpStatus.OK).body(randomLineBackwards);
        }catch (NoFileLineFoundException e){
            log.warn("No file line found");
            return ResponseEntity.notFound().build();
        }catch (Exception e){
            log.error("Internal server error: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }


    @GetMapping("/longest-lines")
    public ResponseEntity<List<LineResponse>> getLongestFileLines() {
        try{
            log.info("Received request to get longest file lines");
            List<LineResponse> longestLines = fileLineService.getLongestFileLines();
            log.info("Longest file lines retrieved successfully");
            return ResponseEntity.status(HttpStatus.OK).body(longestLines);
        }catch (Exception e){
            log.error("Internal server error: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private ResponseEntity<LineWithMostCommonCharResponse> getRandomLine() {
        try {
            log.info("Received request to get random line with most common character");
            LineWithMostCommonCharResponse lineResponse = fileLineService.getRandomLine();
            log.info("Random line with most common character retrieved successfully");
            return ResponseEntity.status(HttpStatus.OK).body(lineResponse);
        } catch (NoFileLineFoundException e){
            log.warn("No file line found");
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            log.error("Internal server error: {}", e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
