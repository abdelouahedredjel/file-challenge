package com.cocus.redonel.filechallenge.mapper;


import com.cocus.redonel.filechallenge.dto.LineResponse;
import com.cocus.redonel.filechallenge.dto.LineWithMostCommonCharResponse;
import com.cocus.redonel.filechallenge.dto.ReverseLineResponse;
import com.cocus.redonel.filechallenge.model.FileLine;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FileLineMapper {

    private final ModelMapper modelMapper;

    public LineResponse mapToLineResponse(FileLine fileLine){
        LineResponse lineResponse = modelMapper.map(fileLine, LineResponse.class);
        lineResponse.setFileName(fileLine.getFileModel().getFileName());
        return lineResponse;
    }

    public ReverseLineResponse mapToReverseLineResponse(FileLine fileLine, String reverseLine){
        LineResponse lineResponse = mapToLineResponse(fileLine);
        return ReverseLineResponse.builder()
                .lineResponse(lineResponse)
                .reverseLine(reverseLine)
                .build();
    }

    public LineWithMostCommonCharResponse mapToLineWithMostCommonCharResponse(FileLine fileLine, Character mostCommonChar){
        LineResponse lineResponse = mapToLineResponse(fileLine);
        return LineWithMostCommonCharResponse.builder()
                .lineResponse(lineResponse)
                .mostCommonChar(mostCommonChar)
                .build();
    }
}
