package com.cocus.redonel.filechallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(FileChallengeApplication.class, args);
	}

}
