package com.cocus.redonel.filechallenge.helper;

public class StringHelper {

    public static char findMostCommonChar(String line) {
        int[] charCounts = new int[256];
        for (char c : line.toCharArray()) {
            charCounts[c]++;
        }
        int maxCount = 0;
        char mostCommonChar = '\0';
        for (int i = 0; i < charCounts.length; i++) {
            if (charCounts[i] > maxCount) {
                maxCount = charCounts[i];
                mostCommonChar = (char) i;
            }
        }
        return mostCommonChar;
    }
}
