package com.cocus.redonel.filechallenge.exception;

public class NoFileFoundException extends RuntimeException {
    public NoFileFoundException(String message) {
        super(message);
    }
}
