package com.cocus.redonel.filechallenge.exception;

public class NoFileLineFoundException extends RuntimeException{
    public NoFileLineFoundException(String exception){
        super(exception);
    }
}
