package com.cocus.redonel.filechallenge.controller;

import com.cocus.redonel.filechallenge.dto.LineResponse;
import com.cocus.redonel.filechallenge.dto.LineWithMostCommonCharResponse;
import com.cocus.redonel.filechallenge.dto.ReverseLineResponse;
import com.cocus.redonel.filechallenge.exception.NoFileLineFoundException;
import com.cocus.redonel.filechallenge.service.FileLineService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class FileLineControllerTest {

    @Mock
    private FileLineService fileLineService;

    @InjectMocks
    private FileLineController fileLineController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetRandomLineText_Success() {
        String randomLine = "Random line";
        when(fileLineService.getRandomLineText()).thenReturn(randomLine);
        ResponseEntity<String> responseEntity = fileLineController.getRandomLineText();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(randomLine, responseEntity.getBody());
        verify(fileLineService, times(1)).getRandomLineText();
    }

    @Test
    void testGetRandomLineText_NoFileLineFound() {
        when(fileLineService.getRandomLineText()).thenThrow(new NoFileLineFoundException("No file line found"));
        ResponseEntity<String> responseEntity = fileLineController.getRandomLineText();

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        verify(fileLineService, times(1)).getRandomLineText();
    }

    @Test
    void testGetRandomLineText_InternalServerError() {
        when(fileLineService.getRandomLineText()).thenThrow(new RuntimeException("Internal Server Error"));
        ResponseEntity<String> responseEntity = fileLineController.getRandomLineText();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        verify(fileLineService, times(1)).getRandomLineText();
    }

    @Test
    void testGetRandomLine_Success() {
        LineWithMostCommonCharResponse lineResponse = new LineWithMostCommonCharResponse();
        when(fileLineService.getRandomLine()).thenReturn(lineResponse);
        ResponseEntity<LineWithMostCommonCharResponse> responseEntity = fileLineController.getRandomLineAsJson();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(lineResponse, responseEntity.getBody());
        verify(fileLineService, times(1)).getRandomLine();
    }

    @Test
    void testGetRandomLine_NoFileLineFound() {
        when(fileLineService.getRandomLine()).thenThrow(new NoFileLineFoundException("No file line found"));
        ResponseEntity<LineWithMostCommonCharResponse> responseEntity = fileLineController.getRandomLineAsJson();

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        verify(fileLineService, times(1)).getRandomLine();
    }

    @Test
    void testGetRandomLine_InternalServerError() {
        when(fileLineService.getRandomLine()).thenThrow(new RuntimeException("Internal Server Error"));
        ResponseEntity<LineWithMostCommonCharResponse> responseEntity = fileLineController.getRandomLineAsJson();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        verify(fileLineService, times(1)).getRandomLine();
    }

    @Test
    void testGetRandomLineBackwardsText_Success() {
        ReverseLineResponse reverseLineResponse = new ReverseLineResponse();
        when(fileLineService.getRandomLineBackwards()).thenReturn(reverseLineResponse);
        ResponseEntity<ReverseLineResponse> responseEntity = fileLineController.getRandomLineBackwardsText();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(reverseLineResponse, responseEntity.getBody());
        verify(fileLineService, times(1)).getRandomLineBackwards();
    }

    @Test
    void testGetRandomLineBackwardsText_NoFileLineFound() {
        when(fileLineService.getRandomLineBackwards()).thenThrow(new NoFileLineFoundException("No file line found"));
        ResponseEntity<ReverseLineResponse> responseEntity = fileLineController.getRandomLineBackwardsText();

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        verify(fileLineService, times(1)).getRandomLineBackwards();
    }

    @Test
    void testGetRandomLineBackwardsText_InternalServerError() {
        when(fileLineService.getRandomLineBackwards()).thenThrow(new RuntimeException("Internal Server Error"));
        ResponseEntity<ReverseLineResponse> responseEntity = fileLineController.getRandomLineBackwardsText();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        verify(fileLineService, times(1)).getRandomLineBackwards();
    }

    @Test
    void testGetLongestFileLines_Success() {
        List<LineResponse> longestLines = Collections.singletonList(new LineResponse());
        when(fileLineService.getLongestFileLines()).thenReturn(longestLines);
        ResponseEntity<List<LineResponse>> responseEntity = fileLineController.getLongestFileLines();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(longestLines, responseEntity.getBody());
        verify(fileLineService, times(1)).getLongestFileLines();
    }

    @Test
    void testGetLongestFileLines_InternalServerError() {
        when(fileLineService.getLongestFileLines()).thenThrow(new RuntimeException("Internal Server Error"));
        ResponseEntity<List<LineResponse>> responseEntity = fileLineController.getLongestFileLines();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        verify(fileLineService, times(1)).getLongestFileLines();
    }
}
