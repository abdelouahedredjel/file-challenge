package com.cocus.redonel.filechallenge.controller;

import com.cocus.redonel.filechallenge.dto.LongestFileLinesResponse;
import com.cocus.redonel.filechallenge.exception.NoFileFoundException;
import com.cocus.redonel.filechallenge.service.FileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class FileControllerTest {

    @Mock
    private FileService fileService;

    private FileController fileController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        fileController = new FileController(fileService);
    }

    @Test
    void testUploadFile_Success() throws IOException {
        String fileName = "testFile.txt";
        ResponseEntity<String> responseEntity = fileController.uploadFile(fileName);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("File uploaded successfully.", responseEntity.getBody());
        verify(fileService, times(1)).uploadFile(fileName);
    }

    @Test
    void testUploadFile_Failure() throws IOException {
        String fileName = "testFile.txt";
        IOException exception = new IOException("File not found");
        doThrow(exception).when(fileService).uploadFile(fileName);
        ResponseEntity<String> responseEntity = fileController.uploadFile(fileName);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        assertEquals("Error uploading file: File not found", responseEntity.getBody());
        verify(fileService, times(1)).uploadFile(fileName);
    }

    @Test
    void testGetLimitedNumberOfLongestLinesOfRandomFile_Success() {
        LongestFileLinesResponse mockResponse = LongestFileLinesResponse.builder()
                .fileName("testFile.txt")
                .longestLines(List.of("Long line 1", "Long line 2"))
                .build();
        when(fileService.getLimitedNumberOfLongestLinesOfRandomFile()).thenReturn(mockResponse);
        ResponseEntity<LongestFileLinesResponse> responseEntity = fileController.getLimitedNumberOfLongestLinesOfRandomFile();

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("testFile.txt", responseEntity.getBody().getFileName());
        assertEquals(2, responseEntity.getBody().getLongestLines().size());
        verify(fileService, times(1)).getLimitedNumberOfLongestLinesOfRandomFile();
    }

    @Test
    void testGetLimitedNumberOfLongestLinesOfRandomFile_NoFileFound() {
        when(fileService.getLimitedNumberOfLongestLinesOfRandomFile()).thenThrow(new NoFileFoundException("No file found"));
        ResponseEntity<LongestFileLinesResponse> responseEntity = fileController.getLimitedNumberOfLongestLinesOfRandomFile();

        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
        verify(fileService, times(1)).getLimitedNumberOfLongestLinesOfRandomFile();
    }

    @Test
    void testGetLimitedNumberOfLongestLinesOfRandomFile_InternalServerError() {
        when(fileService.getLimitedNumberOfLongestLinesOfRandomFile()).thenThrow(new RuntimeException("Internal Server Error"));
        ResponseEntity<LongestFileLinesResponse> responseEntity = fileController.getLimitedNumberOfLongestLinesOfRandomFile();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
        verify(fileService, times(1)).getLimitedNumberOfLongestLinesOfRandomFile();
    }
}
