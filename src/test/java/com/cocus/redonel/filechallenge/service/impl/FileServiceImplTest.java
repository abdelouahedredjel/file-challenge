package com.cocus.redonel.filechallenge.service.impl;

import com.cocus.redonel.filechallenge.dto.LongestFileLinesResponse;
import com.cocus.redonel.filechallenge.model.FileLine;
import com.cocus.redonel.filechallenge.model.FileModel;
import com.cocus.redonel.filechallenge.repository.FileLineRepository;
import com.cocus.redonel.filechallenge.repository.FileRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class FileServiceImplTest {

    @Mock
    private FileRepository fileRepository;

    @Mock
    private FileLineRepository fileLineRepository;

    private FileServiceImpl fileService;

    private static final String SOURCE_DIR = "source-files";
    private static final int NUMBER_OF_LINES_TO_RETRIEVE = 2;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        fileService = new FileServiceImpl(fileRepository, fileLineRepository, NUMBER_OF_LINES_TO_RETRIEVE, SOURCE_DIR);
        ReflectionTestUtils.setField(fileService, "sourceDir", SOURCE_DIR);
    }

    @Test
    void testUploadFile() throws IOException {
        String fileName = "testFile.txt";
        when(fileRepository.findByFileName(anyString())).thenReturn(Optional.empty());
        fileService.uploadFile(fileName);
        verify(fileRepository, times(1)).save(any());
    }

    @Test
    void testGetLimitedNumberOfLongestLinesOfRandomFile() {
        List<String> lines = new ArrayList<>();
        lines.add("short line");
        lines.add("medium length line");
        lines.add("very long line with more characters");
        FileModel fileModel = new FileModel();
        fileModel.setFileName("testFile.txt");
        List<FileLine> fileLines = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
            FileLine fileLine = FileLine.builder()
                    .line(lines.get(i))
                    .lineNumber(i + 1)
                    .fileModel(fileModel)
                    .build();
            fileLines.add(fileLine);
        }
        fileModel.setLines(fileLines);

        when(fileRepository.findRandomFile()).thenReturn(java.util.Optional.of(fileModel));
        LongestFileLinesResponse response = fileService.getLimitedNumberOfLongestLinesOfRandomFile();

        assertEquals("testFile.txt", response.getFileName());
        assertEquals(2, response.getLongestLines().size());
    }
}
