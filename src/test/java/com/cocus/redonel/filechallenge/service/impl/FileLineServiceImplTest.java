package com.cocus.redonel.filechallenge.service.impl;

import com.cocus.redonel.filechallenge.dto.LineResponse;
import com.cocus.redonel.filechallenge.dto.LineWithMostCommonCharResponse;
import com.cocus.redonel.filechallenge.dto.ReverseLineResponse;
import com.cocus.redonel.filechallenge.exception.NoFileLineFoundException;
import com.cocus.redonel.filechallenge.mapper.FileLineMapper;
import com.cocus.redonel.filechallenge.model.FileLine;
import com.cocus.redonel.filechallenge.model.FileModel;
import com.cocus.redonel.filechallenge.repository.FileLineRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

class FileLineServiceImplTest {

    @Mock
    private FileLineRepository fileLineRepository;

    @Mock
    private FileLineMapper fileLineMapper;

    @InjectMocks
    private FileLineServiceImpl fileLineService;

    private FileLine fileLine;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        fileLine = new FileLine();
        fileLine.setLine("Test line");
        fileLine.setLineNumber(1);
        fileLine.setFileModel(FileModel.builder().fileName("Test file").build());
    }

    @Test
    void testGetRandomLineText() {
        when(fileLineRepository.count()).thenReturn(1L);
        when(fileLineRepository.findRandomLine(anyLong())).thenReturn(fileLine);

        String randomLine = fileLineService.getRandomLineText();
        assertNotNull(randomLine);
    }

    @Test
    void testGetRandomLineText_NoFileLineFound() {
        when(fileLineRepository.count()).thenReturn(0L);
        assertThrows(NoFileLineFoundException.class, () -> fileLineService.getRandomLineText());
    }

    @Test
    void testGetRandomLine() {
        when(fileLineRepository.count()).thenReturn(1L);
        when(fileLineRepository.findRandomLine(anyLong())).thenReturn(fileLine);
        when(fileLineMapper.mapToLineWithMostCommonCharResponse(any(), anyChar()))
                .thenReturn(LineWithMostCommonCharResponse.builder().build());

        LineWithMostCommonCharResponse response = fileLineService.getRandomLine();

        assertNotNull(response);
    }

    @Test
    void testGetRandomLine_NoFileLineFound() {
        when(fileLineRepository.count()).thenReturn(0L);
        assertThrows(NoFileLineFoundException.class, () -> fileLineService.getRandomLine());
    }

    @Test
    void testGetRandomLineBackwards() {
        when(fileLineRepository.count()).thenReturn(1L);
        when(fileLineRepository.findRandomLine(anyLong())).thenReturn(fileLine);
        when(fileLineMapper.mapToReverseLineResponse(any(), anyString()))
                .thenReturn(ReverseLineResponse.builder().build());

        ReverseLineResponse response = fileLineService.getRandomLineBackwards();

        assertNotNull(response);
    }

    @Test
    void testGetRandomLineBackwards_NoFileLineFound() {
        when(fileLineRepository.count()).thenReturn(0L);
        assertThrows(NoFileLineFoundException.class, () -> fileLineService.getRandomLineBackwards());
    }

    @Test
    void testGetLongestFileLines() {
        List<FileLine> fileLines = new ArrayList<>();
        fileLines.add(new FileLine());
        when(fileLineRepository.findTopLinesOrderedByLineLengthDesc(3)).thenReturn(fileLines);

        List<LineResponse> longestLines = fileLineService.getLongestFileLines();

        assertNotNull(longestLines);
    }
}
