package com.cocus.redonel.filechallenge.helper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringHelperTest {

    @Test
    void testFindMostCommonChar_EmptyString() {
        String input = "";
        char result = StringHelper.findMostCommonChar(input);
        assertEquals('\0', result);
    }

    @Test
    void testFindMostCommonChar_SingleChar() {
        String input = "a";
        char result = StringHelper.findMostCommonChar(input);
        assertEquals('a', result);
    }

    @Test
    void testFindMostCommonChar_MultipleChars() {
        String input = "abbcccddddeeeee";
        char result = StringHelper.findMostCommonChar(input);
        assertEquals('e', result);
    }

    @Test
    void testFindMostCommonChar_MultipleCharsEqualCount() {
        String input = "abcde";
        char result = StringHelper.findMostCommonChar(input);
        assertEquals('a', result);
    }
}
