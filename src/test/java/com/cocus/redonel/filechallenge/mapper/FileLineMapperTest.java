package com.cocus.redonel.filechallenge.mapper;

import com.cocus.redonel.filechallenge.dto.LineResponse;
import com.cocus.redonel.filechallenge.dto.LineWithMostCommonCharResponse;
import com.cocus.redonel.filechallenge.dto.ReverseLineResponse;
import com.cocus.redonel.filechallenge.model.FileLine;
import com.cocus.redonel.filechallenge.model.FileModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class FileLineMapperTest {

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private FileLineMapper fileLineMapper;

    private FileLine fileLine;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        fileLine = new FileLine();
        fileLine.setLine("Test line");
        fileLine.setLineNumber(1);
        fileLine.setFileModel(new FileModel());
    }

    @Test
    void testMapToLineResponse() {
        LineResponse expectedResponse = new LineResponse("Test line", 1, null);
        when(modelMapper.map(fileLine, LineResponse.class)).thenReturn(expectedResponse);
        LineResponse result = fileLineMapper.mapToLineResponse(fileLine);

        assertEquals(expectedResponse, result);
        assertEquals(fileLine.getFileModel().getFileName(), result.getFileName());
    }

    @Test
    void testMapToReverseLineResponse() {
        String reverseLine = "enil tseT";
        LineResponse expectedResponse = new LineResponse("Test line", 1, null);
        ReverseLineResponse expectedReverseResponse = new ReverseLineResponse(expectedResponse, reverseLine);

        when(modelMapper.map(fileLine, LineResponse.class)).thenReturn(expectedResponse);
        ReverseLineResponse result = fileLineMapper.mapToReverseLineResponse(fileLine, reverseLine);

        assertEquals(expectedReverseResponse, result);
        assertEquals(fileLine.getFileModel().getFileName(), result.getLineResponse().getFileName());
    }

    @Test
    void testMapToLineWithMostCommonCharResponse() {
        Character mostCommonChar = 't';
        LineResponse expectedResponse = new LineResponse("Test line", 1, null);
        LineWithMostCommonCharResponse expectedWithCommonChar = new LineWithMostCommonCharResponse(expectedResponse, mostCommonChar);

        when(modelMapper.map(fileLine, LineResponse.class)).thenReturn(expectedResponse);
        LineWithMostCommonCharResponse result = fileLineMapper.mapToLineWithMostCommonCharResponse(fileLine, mostCommonChar);

        assertEquals(expectedWithCommonChar, result);
        assertEquals(fileLine.getFileModel().getFileName(), result.getLineResponse().getFileName());
    }

}